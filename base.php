<?php get_template_part('templates/head'); ?>
<?php $bodyClass = is_tree(); ?>
<body <?php body_class($bodyClass); ?>>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KBR2PPT" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  
  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'roots'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    get_template_part('templates/header');
  ?>

  <div class="wrap container" role="document">
    <div class="content row">
      <?php if(have_rows('banner') && is_page()): ?>
    <?php
      get_template_part('templates/home-slider');
    ?>
      <?php endif; ?>
      <?php if(is_front_page()): ?>
      <div class="top-banner-block">
        <!-- <a href="<?php //bloginfo('url'); ?>/products/act-kids/act-kids-toothpaste/"><img src="<?php //bloginfo('url'); ?>/media/Narrow-Banner-kids-toothpaste.jpg" class="img-responsive" /></a> -->
        <a href="<?php bloginfo('url'); ?>/products/act-advanced-care/"><img src="<?php bloginfo('url'); ?>/media/home-top-banner.jpg" class="img-responsive" /></a>
      </div>
    <?php endif; ?>
      <?php if(is_search() || is_archive() || is_single() || is_home()): ?>
      <div class="header-full-width">
      <?php get_template_part('templates/page', 'header'); ?>
      </div>
      <?php endif; ?>
      <main class="main" role="main">
        <?php include roots_template_path(); ?>
      </main><!-- /.main -->
      <?php if (roots_display_sidebar()) : ?>
        <aside class="sidebar" role="complementary">
          <?php include roots_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      <?php endif; ?>
    </div><!-- /.content -->
  </div><!-- /.wrap -->

  <?php get_template_part('templates/footer'); ?>
  <?php wp_footer(); ?>
  <?php
    if( have_rows('tracking_pixel') ):
    while ( have_rows('tracking_pixel') ) : the_row();
  $cachebuster = mt_rand(100000, 999999);
  ?>
  <?php if(get_sub_field('javascript')): ?><?php the_sub_field('javascript'); ?><?php endif; ?>
  <?php if(get_sub_field('noscript')) echo '<noscript>'; ?><img src="<?php the_sub_field('pixel_url'); ?><?php if(get_sub_field('cachebuster')) echo 'rand=' . $cachebuster; ?>" width="1" height="1" border="0"><?php if(get_sub_field('noscript')) echo '</noscript>'; ?>
  <?php endwhile;endif; ?>
  <?php if(get_field('misc-javascript')) the_field('misc-javascript'); ?>
</body>
</html>
