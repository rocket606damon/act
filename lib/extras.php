<?php
/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more() {
  //return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}
add_filter('excerpt_more', 'roots_excerpt_more');

/**
 * Change default uploads to "media"
 */
update_option('uploads_use_yearmonth_folders', 0);
update_option('upload_path', 'media');

/**
 * Remove UberMenu CSS
 */
remove_action( 'wp_head' , 'ubermenu_inject_custom_css' );

function is_tree(){
  $class = '';
  if( is_page() ) { 
  global $post;
  $parents = get_post_ancestors( $post->ID );
  $id = ($parents) ? $parents[count($parents)-1]: $post->ID;
  $parent = get_page( $id );
  $class = $parent->post_name;
}
return $class;
}

/**
 * Register ACF Options Page
 */ 
if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'General Settings',
    'menu_title'  => 'General Settings',
    'menu_slug'   => 'general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Ad Block Settings',
    'menu_title'  => 'Ad Block Settings',
    'parent_slug' => 'general-settings',
  ));
  
}

/**
 * Get Top Most Ancestor
 */
if(!function_exists('get_post_top_ancestor_id')){
	function get_post_top_ancestor_id(){
		global $post;
		if($post->post_parent){
			$ancestors = array_reverse(get_post_ancestors($post->ID));
			return $ancestors[0];
		}
		return $post->ID;
	}
}

/**
 * Make WYSWIG Images Responsive
 */
function add_image_class($class){
	$class .= ' img-responsive';
	return $class;
}
add_filter('get_image_tag_class','add_image_class');

/**
 * Lower SEO Meta Box Priority
 */
function lower_wpseo_priority( $html ) {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'lower_wpseo_priority' );

/**
* Add Superscript & Subscript to MCE
*/
function my_mce_buttons_2($buttons) {	
	
	$buttons[] = 'superscript';
	$buttons[] = 'subscript';

	return $buttons;
}
add_filter('mce_buttons_2', 'my_mce_buttons_2');

/**
* Add Custom Tooltip Button
*/

// Filter Functions with Hooks
function custom_mce_button() {
  // Check if user have permission
  if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
    return;
  }
  // Check if WYSIWYG is enabled
  if ( 'true' == get_user_option( 'rich_editing' ) ) {
    add_filter( 'mce_external_plugins', 'custom_tinymce_plugin' );
    add_filter( 'mce_buttons', 'register_mce_button' );
  }
}
add_action('admin_head', 'custom_mce_button');

// Function for new button
function custom_tinymce_plugin( $plugin_array ) {
  $plugin_array['custom_mce_button'] = get_template_directory_uri() .'/assets/js/tooltip_mce_plugin.js';
  return $plugin_array;
}

// Register new button in the editor
function register_mce_button( $buttons ) {
  array_push( $buttons, 'custom_mce_button' );
  return $buttons;
}

// Add Shortcode
function tooltip_shortcode( $atts , $content = null ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'class' => '',
			'title' => '',
		), $atts )
	);

// Code
return '<span class="tooltip-hover" data-toggle="tooltip" data-placement="'.$class.'" title="'.$title.'">'.$content.'</span>';
}
add_shortcode( 'tooltip', 'tooltip_shortcode' );

function load_custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/assets/css/custom-editor-style.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

/**
* Confirm Registration
*/

class GW_Value_Exists_Validation {

    public function __construct( $args = array() ) {

        // set our default arguments, parse against the provided arguments, and store for use throughout the class
        $this->_args = wp_parse_args( $args, array(
	        'target_form_id'  => false,
	        'target_field_id' => false,
	        'source_form_id'  => false,
	        'source_field_id' => false,
	        'validation_message' => __( 'Please enter a valid value.' )
        ) );

        // do version check in the init to make sure if GF is going to be loaded, it is already loaded
        add_action( 'init', array( $this, 'init' ) );

    }

    public function init() {

        // make sure we're running the required minimum version of Gravity Forms
        if( ! property_exists( 'GFCommon', 'version' ) || ! version_compare( GFCommon::$version, '1.8', '>=' ) ) {
            return;
        }

		add_filter( 'gform_validation', array( $this, 'validation' ) );

    }

	public function validation( $result ) {

		if( ! $this->is_applicable_form( $result['form'] ) ) {
			return $result;
		}

		foreach( $result['form']['fields'] as &$field ) {

			if( $field['id'] == $this->_args['target_field_id'] ) {
				$value = rgpost( 'input_' . $field['id'] );
				if( ! $this->does_value_exist( $value, $this->_args['source_form_id'], $this->_args['source_field_id'] ) ) {
					$field['failed_validation'] = true;
					$field['validation_message'] = $this->_args['validation_message'];
					$result['is_valid'] = false;
				}
			}

		}

		if($result) {
			$_POST['input_3'] = 'Yes';
		} else {
			$_POST['input_3'] = 'No';
		}

		return $result;
	}

	function does_value_exist( $value, $form_id, $field_id ) {

		$entries = GFAPI::get_entries( $form_id, array( 'field_filters' => array( array( 'key' => $field_id, 'value' => $value ) ) ) );

		return count( $entries ) > 0;
	}

	function is_applicable_form( $form ) {

		$form_id = isset( $form['id'] ) ? $form['id'] : $form;

		return $form_id == $this->_args['target_form_id'];
	}

}

// Configuration

new GW_Value_Exists_Validation( array(
	'target_form_id'  => 5,
	'target_field_id' => 1,
	'source_form_id'  => 3,
	'source_field_id' => 4,
	'validation_message' => 'Sorry, that email isn\'t registered. Please register to print your coupon.'
) );

/**
 * Flexible Content Excerpt
 */
function flexible_content_excerpt(){
	
	if( have_rows('content') ): $count = 0;
	while ( have_rows('content') ) : the_row();
		if( get_row_layout() == 'content_block' ):
			$content = get_sub_field('content');
			$content = strip_tags($content, '<a><strong><em>');
			$content = wp_trim_words($content);
			$content = '<p>' . $content . '</p>';
			$excerpt[$count] = $content;
		elseif( get_row_layout() == 'image_blocks' ):
            	if( have_rows('image_block') ): $count = 0;
                while ( have_rows('image_block') ) : the_row();
					$content = get_sub_field('content');
					$content = strip_tags($content, '<a><strong><em>');
					$content = wp_trim_words($content);
					$content = '<p>' . $content . '</p>';
					$excerpt[$count] = $content;
				 $count++; endwhile; endif;
		endif;
	$count++;endwhile; endif; 
	return $excerpt[0];
	
}
