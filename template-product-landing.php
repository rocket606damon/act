<?php
/*
Template Name: Product Landing
*/
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'product-landing'); ?>
<?php endwhile; ?>
