(function() {
tinymce.PluginManager.add('custom_mce_button2', function(editor, url) {
        editor.addButton('custom_mce_button2', {
            icon: 'icon dashicons-money',
            text: 'Popover',
            onclick: function() {
                editor.windowManager.open({
                    title: 'Insert Popover',
                    body: [{
                        type: 'textbox',
                        name: 'textboxPopover',
                        label: 'Popover Image',
                        value: ''
                    }, ],
                    onsubmit: function(e) {
                        editor.insertContent(
                            '[popover image=&quot;' +
                            e.data.textboxPopover +
                            '&quot;]' +
                            editor.selection
                            .getContent() +
                            '[/tooltip]'
                        );
                    }
                });
            }
        });
    });
})();