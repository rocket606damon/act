/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can 
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function($) {

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
var Roots = {
  // All pages
  common: {
    init: function() {
      // JavaScript to be fired on all pages
	  $('[data-toggle="tooltip"]').tooltip();
	  $('[data-toggle="popover"]').popover({
			html: true,
			trigger: 'hover',
			content: function () {
				return '<img class="img-responsive" src="'+$(this).data('img') + '" />';
	  		}
	  });
	  $(".confirm").confirm();
	  $.confirm.options = {
		  text: "<p>Links to websites that are not under the control of Chattem are provided for convenience of reference only and are not intended as an endorsement by Chattem of the organization or a warranty of any type regarding the information on the website.</p><p>Chattem does not and cannot review all communications and materials posted or uploaded to other websites and is not responsible for the content of these communications and materials.</p><p>Chattem, Inc. is only responsible for web content produced in association with Chattem, Inc.</p><p>Click OK to proceed.</p>",
		  title: "You are about to leave the actoralcare.com website.",
		  confirmButton: "Ok",
		  cancelButton: "Cancel",
		  confirmButtonClass: "btn-success",
		  cancelButtonClass: "btn-default"
	  };
	  function sharePopup(url){
	  	var width = 600;
		var height = 400;
		var leftPosition, topPosition;
		leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
		topPosition = (window.screen.height / 2) - ((height / 2) + 50);
		var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
	  	window.open(url,'Social Share', windowFeatures);
	  }
		
	  $('#share-facebook').on('click', function(){
		var u = location.href;
		var t = document.title;
		sharePopup('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t));
		return false;
	  });
		
	  $('#share-twitter').on('click', function(){
		var u = location.href;
		var t = document.title+' - ';
		sharePopup('http://twitter.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t));
		return false;
	  });
		
	  $('#share-google').on('click', function(){
		var u = location.href;
		var t = document.title;
		sharePopup('https://plus.google.com/share?url='+encodeURIComponent(u)+'&text='+encodeURIComponent(t));
		return false;
	  });
	  
    }
  },
  // Home page
  home: {
    init: function() {
      // JavaScript to be fired on the home page
    }
  },
  plaque_gingivitis: {
    init: function() {
      // JavaScript to be fired on the about us page
    }
  }
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = Roots;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {
    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });
  }
};

$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
