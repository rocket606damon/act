<div class="col-sm-12">
    <div class="col-sm-6">
        <a class="section-btn healthy-habits" href="<?php bloginfo('url'); ?>/act-for-kids/healthy-habits/"><i class="icon fa fa-heart"></i>Healthy <strong>Habits</strong><span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x"></i></span></a>
        <a class="section-btn prevent-cavities" href="<?php bloginfo('url'); ?>/coming-soon/"><i class="icon ai icon-act-adults-icon-anticavity"></i>Prevent <strong>Cavities</strong><span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x"></i></span></a>
        <a class="section-btn parent-tips" href="<?php bloginfo('url'); ?>/act-for-kids/parent-tips/"><i class="icon fa fa-lightbulb-o"></i>Parent <strong>Tips</strong><span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x"></i></span></a>
    </div>
    <div class="col-sm-6">
        <a class="section-btn braces-care" href="<?php bloginfo('url'); ?>/act-for-adults/braces-care/"><i class="icon ai icon-act-adults-icon-braces-care"></i>Braces <strong>Care</strong><span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x"></i></span></a>
        <a class="section-btn sign-up-save" href="<?php bloginfo('url'); ?>/act-for-adults/sign-up-save/"><i class="icon fa fa-usd"></i>Sign Up &amp; <strong>Save</strong><span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x"></i></span></a>
        <a class="section-btn usage-guidlines" href="<?php bloginfo('url'); ?>/act-for-kids/usage-guidelines/"><i class="icon fa fa-check"></i>Usage <span>Guidelines</span><span class="fa-stack fa-lg"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-angle-right fa-stack-1x"></i></span></a>
    </div>
</div>