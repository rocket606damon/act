<div class="sanofi-head">
    <div class="container">
    	<a class="sanofi" href="http://www.sanofi.com" target="_blank">Sanofi</a>
    </div>
</div>
<header class="banner navbar navbar-default navbar-static-top" role="banner">
    <div class="container">
      <?php
          $section = is_tree();
          if($section == ('dental-professionals')):
              $logo = 'act-logo-professional.png';
		  elseif($section == ('act-for-kids')):
		  	  $logo = 'act-logo-kids.png';
          else:
              $logo = 'act-logo-consumer.png';
          endif;
      ?>
      <a class="navbar-brand" title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php bloginfo('url'); ?>/media/<?php echo $logo; ?>" alt="<?php bloginfo('name'); ?>" /></a>
      
      <div class="pre-nav">
          <?php get_search_form(); ?>
          <nav class="nav-sec" role="navigation">
            <ul class="pre-menu">
              <li><a href="<?php bloginfo('url'); ?>/act-for-adults/sign-up-save/"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-usd fa-stack-1x fa-inverse"></i></span>Sign Up &amp; Save</a></li>
              <li><a href="<?php bloginfo('url'); ?>/contact-us/"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-phone fa-stack-1x fa-inverse"></i></span>Contact Us</a></li>
            </ul>
          </nav>
      </div>
      
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav nav-justified'));
        endif;
      ?>
        
    </div>
</header>
