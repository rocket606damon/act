<article <?php post_class(); ?>>
  	<header>
    	<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  	</header>
	<?php 
        $size = 'thumbnail'; // whatever size you want
        $images = get_posts(
            array(
                    'post_type'      => 'attachment',
                    'post_mime_type' => 'image',
                    'post_parent'    => $post->ID,
                    'posts_per_page' => 1,
                )
		);
        if ( $images ) {
        	$thumbnail = wp_get_attachment_image_src( $images[0]->ID, 'thumbnail' );
			$image_url = $thumbnail[0];
        }			
    ?>
	<?php if($image_url != ''): ?>
    <div class="row">
    	<div class="col-sm-3 image">
        	<img class="img-responsive" src="<?php echo $image_url; ?>" alt="<?php the_title(); ?>" />
        </div>
        <div class="col-sm-9 info">
        	<?php the_excerpt(); ?>
            <div class="read-more">
                <a class="btn btn-primary" href="<?php the_permalink(); ?>">Read More</a>
            </div>
        </div>
    </div>
    <?php else: ?>
  	<div class="entry-summary">
    <?php the_excerpt(); ?>
        <div class="read-more">
            <a class="btn btn-primary" href="<?php the_permalink(); ?>">Read More</a>
        </div>
  	</div>
    <?php endif; ?>
</article>
