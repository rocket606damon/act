<div class="content row">
	<div class="col-sm-3 product-menu">
		<h3>Product List</h3>
        <?php wp_nav_menu( array('menu' => 'Product Info' )); ?>
        <a class="btn btn-info btn-block" href="<?php bloginfo('url'); ?>/act-for-adults/where-to-buy/">Where to Buy</a>
    </div>
    <div class="col-sm-9">
    	<div class="row product-landing">
        	<?php
			$args = array(
				'post_type'      => 'page',
				'posts_per_page' => -1,
				'post_parent'    => $post->ID,
				'order'          => 'ASC',
				'orderby'        => 'menu_order'
			 );
			$parent = new WP_Query( $args );
			if ( $parent->have_posts() ) : ?>
				<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
			
				<div class="col-sm-6 block">
                	<div class="image">
                    	<?php if(get_field('image')): ?>
                		<img src="<?php the_field('image'); ?>" class="img-responsive" alt="<?php the_title(); ?>" />
                		<?php endif; ?>
                    </div>
					<h4><?php the_title(); ?></h4>
					<a class="btn btn-info" href="<?php the_permalink(); ?>">Learn More</a>
                </div>
			
				<?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>

    	</div>
    </div>
</div>


