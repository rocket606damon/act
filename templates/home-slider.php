<?php 
	$count = 0; 
	$slides = count(get_field('banner'));
	if($slides > 1):
?>
<div class="carousel-wrap">
    <div class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php for ($i = 0; $i < $slides; $i++): ?>
            <li data-target=".carousel" data-slide-to="<?php echo $i; ?>"<?php if($i == 0) echo ' class="active"'; ?>></li>
            <?php endfor; ?>
        </ol>
        <div class="carousel-inner">
        <?php while ( have_rows('banner') ) : the_row();
            if (get_sub_field('external_link')) { $link = get_sub_field('external_link'); $target="_blank"; } else { $link = get_sub_field('page_link'); $target="_self"; }
            $image = get_sub_field('image');
            $url = $image['url'];
            $size = 'page-banner';
            $banner = $image['sizes'][ $size ];
        ?>
        <div class="item<?php if($count == 0){ echo ' active'; } ?>">
            <a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><img src="<?php echo $banner; ?>" alt="<?php the_sub_field('title'); ?>"></a>
        </div>     
        <?php $count++;endwhile; ?>
        </div>
    </div>
</div>

<?php else: ?>

<div class="carousel-wrap">
        <?php while ( have_rows('banner') ) : the_row();
            if (get_sub_field('external_link')) { $link = get_sub_field('external_link'); $target="_blank"; } else { $link = get_sub_field('page_link'); $target="_self"; }
            $image = get_sub_field('image');
            $url = $image['url'];
            $size = 'page-banner';
            $banner = $image['sizes'][ $size ];
        ?>
        <a href="<?php echo $link; ?>" target="<?php echo $target; ?>"><img class="img-responsive" src="<?php echo $banner; ?>" alt="<?php the_sub_field('title'); ?>"></a>
        <?php endwhile; ?>
</div>

<?php endif; ?>