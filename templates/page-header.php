<div class="page-header">
	<div class="row">
    	<div class="col-sm-10">
  			<h1><?php echo roots_title(); ?></h1>
  			<?php if(get_field('subtitle') && !is_search() && !is_home()) echo '<h3>' . get_field('subtitle') . '</h3>'; ?>
        </div>
        <div class="col-sm-2 social btn-group">
        	<ul>
            	<!-- HIDE SOCIAL SHARING MENU
                <li>
                	<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-share-alt fa-stack-1x fa-inverse"></i></span></a>
                    
                    <ul class="dropdown-menu" role="menu">
                        <li><a id="share-facebook" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a id="share-twitter" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a id="share-google" class="btn btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </li>
                -->
                <li><a href="#" data-toggle="modal" data-target="#email-modal"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></a></li>
                <li><a href="javascript:window.print()"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-print fa-stack-1x fa-inverse"></i></span></a></li>
            </ul>
            <div class="modal fade " id="email-modal">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Email a friend: <?php the_title(); ?></h4>
                  </div>
                  <div class="modal-body">
                    <?php 
						$excerpt = get_the_excerpt();
						gravity_form( 6, false, false, false, array('the_excerpt' => $excerpt), true); 
					?>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
		</div>
    </div>
</div>
<!-- Single button -->
