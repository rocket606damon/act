<div class="content row">
	<?php if(have_rows('product')): $count = 0; ?>
    <div class="row-same-height row-full-height product-row">
	<?php while (have_rows('product')) : the_row(); ?>
    <?php 
		// Variables
		$image = get_sub_field('image');
		$image_url = $image['url'];
		$image_size = 'product-thumb';
		$image_thumb = $image['sizes'][ $image_size ];
	?>
    
<?php if (($count % 4 == 0) && ($count > 1)): ?>
	</div>
</div>
<div class="content row">
	<div class="row-same-height row-full-height product-row">
<?php endif; ?>
        
        <div class="col-sm-3 col-sm-height col-full-height col-product-<?php echo $count; ?>">
            <div class="product">
                <?php if(get_sub_field('new_item')): ?><span class="new">New</span><?php endif; ?>
                <?php if(get_sub_field('new_flavor')): ?><span class="new_flavor">New Flavor</span><?php endif; ?>
                <div class="image"><img src="<?php echo $image_url; ?>" class="img-responsive" alt="<?php the_sub_field('product_family'); ?> - <?php the_sub_field('flavor'); ?>" /></div>
                <h4><?php the_sub_field('product_family'); ?></h4>
                <?php if(get_sub_field('subtitle')): ?><p class="subtitle"><?php the_sub_field('subtitle'); ?></p><?php endif; ?>
                <p class="flavor" style="color: <?php the_sub_field('accent_color'); ?>;"><?php the_sub_field('flavor'); ?></p>
                <div class="buttons">
                    <a class="learn-more col-sm-6" href="<?php the_sub_field('product_page'); ?>">Learn More</a>
                    <a class="where-to-buy col-sm-6" href="<?php bloginfo('url'); ?>/where-to-buy/">Where to Buy</a>
                </div>
            </div>
        </div>
       
            
    <?php $count++; endwhile; ?>
	</div>
    <?php endif; ?>
</div>