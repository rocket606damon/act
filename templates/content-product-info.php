<div class="product-info">
    <div class="col-sm-3 product-menu">
    	<h3>Product List</h3>
        <?php wp_nav_menu( array('menu' => 'Product Info' )); ?>
        <a class="btn btn-info btn-block" href="<?php bloginfo('url'); ?>/act-for-adults/where-to-buy/">Where to Buy</a>
    </div>
    <div class="col-sm-9 product-info">
        <div class="row">
            <div class="col-sm-6 image">
                <?php if(get_field('image')): ?>
                <img src="<?php the_field('image'); ?>" class="img-responsive" alt="<?php the_title(); ?>" />
                <?php endif; ?>
            </div>
            <div class="col-sm-6 info">
                <?php the_field('product_info'); ?>
                <?php if(get_field('drug_facts')): ?>
                <a role="button" href="#" class="drug-facts" data-toggle="modal" data-target="#drug-facts"><?php echo is_page('act-dry-mouth-lozenges') ? 'View Nutrition Facts' : 'View Drug Facts' ?></a>
                <div class="modal fade" id="drug-facts">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      	<h4 class="modal-title"><?php the_title(); ?> Drug Facts</h4>
                      </div>
                      <div class="modal-body">
                        <img class="img-responsive" src="<?php the_field('drug_facts'); ?>" alt="<?php the_title(); ?> Drug Facts" />
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

