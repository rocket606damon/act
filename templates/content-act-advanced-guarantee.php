<div class="top-banner">
<?php the_content(); ?>
<?php if(is_page('act-advanced-guarantee')): ?>
<!--<a class="btn btn-primary" href="<?php bloginfo('url'); ?>/act-for-adults/plaque-gingivitis/">Learn more about plaque &amp; gingivitis</a>-->
<?php endif; ?>
</div>
<div class="row">
	<?php if( have_rows('content_left') ): ?>
    <div class="col-sm-8">
        <?php while (have_rows('content_left')) : the_row(); ?>
        <div class="block<?php if(get_sub_field('bordered')) echo ' bordered'; ?>">
            
			<?php if( get_row_layout() == 'content_block' ): ?>
            
				<?php the_sub_field('content'); ?>
                
			<?php elseif( get_row_layout() == 'image_gallery' ): ?>
            
                <div class="row row-no-padding">
                <?php 
                    if( have_rows('image') ):
                    while ( have_rows('image') ) : the_row();
                ?>
                <div class="col-sm-6">
                    <a class="confirm" href="<?php the_sub_field('link'); ?>" target="_blank"><img class="img-responsive" src="<?php the_sub_field('image'); ?>" /></a>
                </div>            
                <?php endwhile;endif; ?>
                </div>
                
            <?php elseif( get_row_layout() == 'accordian' ): ?>
            	<div class="accordian">
                	<div class="intro">
                	<?php the_sub_field('intro'); ?>
                    </div>
                	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            		<?php if(have_rows('accordian_segment')): $count = 0;
	    			while (have_rows('accordian_segment')) : the_row(); ?>
                	
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><a data-toggle="collapse"<?php if($count != 0) echo ' class="collapsed"' ?> data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a></h3>
                            </div>
                            <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse <?php if($count != 0) { echo 'collapsed'; } else { echo 'in'; } ?>">
                                <div class="panel-body">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            </div>
                        </div>
                        
                    <?php $count++;endwhile;endif; ?>
                    </div>
                </div>
			
			<?php endif; ?>
            
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
    
    <?php if( have_rows('content_right') ): ?>
    <div class="col-sm-4">
        <?php while (have_rows('content_right')) : the_row(); ?>
        <div class="block<?php if(get_sub_field('bordered')) echo ' bordered'; ?>">
        
            <?php if( get_row_layout() == 'content_block' ): ?>
            
				<?php the_sub_field('content'); ?>
                
            <?php  elseif( get_row_layout() == 'image_gallery' ): ?>
            
                <div class="row row-no-padding">
                <?php 
                    if( have_rows('image') ):
                    while ( have_rows('image') ) : the_row();
                ?>
                <div class="col-sm-6">
                    <a class="confirm" href="<?php the_sub_field('link'); ?>" target="_blank"><img class="img-responsive" src="<?php the_sub_field('image'); ?>" /></a>
                </div>            
                <?php endwhile;endif; ?>
                </div>
            <?php elseif( get_row_layout() == 'accordian' ): ?>
            	<div class="accordian">
                	<div class="intro">
                	<?php the_sub_field('intro'); ?>
                    </div>
                	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            		<?php if(have_rows('accordian_segment')): $count = 0;
	    			while (have_rows('accordian_segment')) : the_row(); ?>
                	
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><a data-toggle="collapse"<?php if($count != 0) echo ' class="collapsed"' ?> data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a></h3>
                            </div>
                            <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse <?php if($count != 0) { echo 'collapsed'; } else { echo 'in'; } ?>">
                                <div class="panel-body">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            </div>
                        </div>
                        
                    <?php $count++;endwhile;endif; ?>
                    </div>
                </div>
			
            <?php endif; ?>
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</div>

<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>