<?php
	if(is_tree() == 'dental-professionals'):
		$prefix = 'professional';
	elseif(is_tree() == 'act-for-kids'):
		$prefix = 'kids';
	else:
		$prefix = 'default';
	endif;
	// $prefix = 'default'; //Setup
?>
<?php 
	if( have_rows($prefix . '_block', 'option')):
	$count = 0; 
?>
<div class="wrap container info-blocks">
    <div class="content row">
    	<div class="row-same-height row-full-height">
        
        <!--Dental Professional Blocks -->
		<?php if($prefix == 'professional'): ?>
			<?php while ( have_rows($prefix . '_block', 'option') ) : the_row(); ?>
                
                <?php
                    if(get_sub_field($prefix . '_external_link')):
                        $link = get_sub_field($prefix . '_external_link');
                        $target = "_blank";
                    else:
                        $link = get_sub_field($prefix . '_page_link');
                        $target = "_self";
                    endif;
                ?>
                <?php if($count == 0): ?>
                <div class="col-sm-4 col-sm-height col-full-height block">
                    <div class="col-xs-7 image">
                        <img class="img-responsive" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-5 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php elseif($count == 1): ?>
                <div class="col-sm-4 col-sm-height col-full-height block">
                    <div class="col-xs-5 image">
                        <img class="img-responsive" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-7 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php else: ?>
                <div class="col-sm-3 col-sm-height col-full-height block">
                    <div class="col-xs-5 image">
                        <img class="img-responsive" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-7 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php endif; ?>
            <?php $count++; endwhile; ?>

        <!-- Kids Blocks -->
        <?php elseif($prefix == 'kids'): ?>
            <?php while ( have_rows($prefix . '_block', 'option') ) : the_row(); ?>
                
                <?php
                    if(get_sub_field($prefix . '_external_link')):
                        $link = get_sub_field($prefix . '_external_link');
                        $target = "_blank";
                    else:
                        $link = get_sub_field($prefix . '_page_link');
                        $target = "_self";
                    endif;
                ?>
                <?php if($count == 0): ?>
                <div class="col-sm-3 col-sm-height col-full-height block">
                    <div class="col-xs-5 image">
                        <img class="img-responsive kid-block-<?php echo $count; ?>" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-7 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php elseif($count == 1): ?>
                <div class="col-sm-4 col-sm-height col-full-height block">
                    <div class="col-xs-6 image">
                        <img class="img-responsive kid-block-<?php echo $count; ?>" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-6 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php else: ?>
                <div class="col-sm-3 col-sm-height col-full-height block adblock-clear">
                    <div class="col-xs-5 image">
                        <img class="img-responsive kid-block-<?php echo $count; ?>" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-7 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php endif; ?>
            <?php $count++; endwhile; ?>    
        
		<!-- Default -->
		<?php else: ?>
			<?php while ( have_rows($prefix . '_block', 'option') ) : the_row(); ?>
                
                <?php
                    if(get_sub_field($prefix . '_external_link')):
                        $link = get_sub_field($prefix . '_external_link');
                        $target = "_blank";
                    else:
                        $link = get_sub_field($prefix . '_page_link');
                        $target = "_self";
                    endif;
                ?>
                <?php if($count == 0): ?>
                <div class="col-sm-4 col-sm-height col-full-height block">
                    <div class="col-xs-7 image">
                        <img class="img-responsive" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-5 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php elseif($count == 1): ?>
                <div class="col-sm-4 col-sm-height col-full-height block">
                    <div class="col-xs-4 image">
                        <img class="img-responsive" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-8 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php else: ?>  
                <div class="col-sm-4 col-sm-height col-full-height block adblock-clear">
                    <div class="col-xs-4 image">
                        <img class="img-responsive" src="<?php the_sub_field($prefix . '_image'); ?>" alt="<?php the_sub_field($prefix . '_title'); ?>" />
                    </div>
                    <div class="col-xs-8 info">
                        <?php if(get_sub_field($prefix . '_new_item')): ?><span class="new">New</span><?php endif; ?>
                        <h3><?php the_sub_field($prefix . '_title'); ?></h3>
                        <p><?php the_sub_field($prefix . '_text'); ?></p>
                        <a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">Learn More</a>
                    </div>
                </div>
                <?php endif; ?>
            <?php $count++; endwhile; ?>
        <?php endif; ?>
        </div>
    </div>
</div>
<?php endif; ?>

<footer class="content-info" role="contentinfo">
  <div class="container">
  		<div class="row">
        	<div class="col-md-12 col-lg-12 footer-menu">
            	<div class="col-sm-6 col-md-6 col-lg-3"><a href="<?php bloginfo('url'); ?>/products/">Products<i class="fa fa-chevron-circle-right"></i></a></div>
                <div class="col-sm-6 col-md-6 col-lg-3"><a href="<?php bloginfo('url'); ?>/act-for-adults/">ACT® For Adults<i class="fa fa-chevron-circle-right"></i></a></div>
                <div class="col-sm-6 col-md-6 col-lg-3"><a href="<?php bloginfo('url'); ?>/act-for-kids/">ACT® For Kids<i class="fa fa-chevron-circle-right"></i></a></div>
                <div class="col-sm-6 col-md-6 col-lg-3"><a href="<?php bloginfo('url'); ?>/dental-professionals/">Dental Professionals<i class="fa fa-chevron-circle-right"></i></a></div>
        	</div>
        </div>
  </div>
  <div class="sub-footer">
  	<div class="container">
    	<div class="row row-no-padding">
        	<div class="col-sm-12 col-md-8">
                <ul class="legal">
                    <li>&copy; <?php echo date("Y") ?> Chattem, Inc.</li>
                    <li><a href="http://chattem.com/privacy.asp" target="_blank">Privacy Policy</a></li>
                    <li><a href="http://chattem.com/termsofuse.asp" target="_blank">Terms of Use</a></li>
                    <li><a href="<?php bloginfo('url'); ?>/contact-us/">Contact Us</a></li>
                </ul>
        	</div>
            <div class="col-sm-12 col-md-4">
            	<ul class="branding">
                    <li><a class="chattem" href="http://www.chattem.com" target="_blank">Chattem</a></li>
                    <li><a class="sanofi" href="http://www.sanofi.com" target="_blank">Sanofi</a></li>
                    <li><a class="launchpad" href="http://www.launchpad.co" target="_blank">LaunchPad Media LLC</a></li>
                </ul>
            </div>
        </div>
    </div>
  </div>
</footer>
