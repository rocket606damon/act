<!-- Determines what to do-->
<?php if($_GET['exists'] == 'Yes'):
  the_content();
  else: 
  echo "You need a special link in order to access this page.";
endif; 
?>

<?php if(is_tree() == 'dental-professionals'): ?>

  <?php if( have_rows('content_block') ): $last = count(get_field('content_block')); ?>
    <?php $count = 1; while (have_rows('content_block')) : the_row(); ?>
    <?php if($count == $last) { $lastly = ' no-border'; } else { $lastly = ''; } ?>
        <?php if(get_row_layout() == 'even_columns'): ?>
        <div class="row block<?php echo $lastly; ?>">
          <?php if(get_sub_field('justify_content') == 'left') : ?>
          <div class="col-sm-6 info">
              <?php the_sub_field('content'); ?>
            </div>
            <div class="col-sm-6 image">
              <img class="img-responsive" src="<?php the_sub_field('image'); ?>" />
            </div>
      <?php else: ?>
            <div class="col-sm-6 image">
              <img class="img-responsive" src="<?php the_sub_field('image'); ?>" />
            </div>
            <div class="col-sm-6 info">
              <?php the_sub_field('content'); ?>
            </div>
            <?php endif; ?>
        </div>
    <?php elseif(get_row_layout() == 'uneven_columns'): ?>
        <div class="row block<?php echo $lastly; ?>" <?php if($count == 5){ echo "style='display:none;'"; } ?>>
          <?php if(get_sub_field('justify_content') == 'left') : ?>
          <div class="col-sm-8 info">
              <?php the_sub_field('content'); ?>
            </div>
            <div class="col-sm-4 image">
              <img class="img-responsive" src="<?php the_sub_field('image'); ?>" />
            </div>
      <?php else: ?>
            <div class="col-sm-4 image">
              <img class="img-responsive" src="<?php the_sub_field('image'); ?>" />
            </div>
            <div class="col-sm-8 info">
              <?php the_sub_field('content'); ?>
            </div>
            <?php endif; ?>
      </div>
        <?php elseif(get_row_layout() == 'full_width'): ?>
        <div class="row block<?php echo $lastly; ?>">
          <div class="col-sm-12 info">
              <?php the_sub_field('content'); ?>
            </div>
      </div>
    <?php endif; ?>
  <?php $count++;endwhile; ?>
    <?php endif; ?>
    <div class="row">
      <div class="col-sm-12">
        <a class="back-to-top" href="#">Back <strong>To Top</strong><i class="fa fa-chevron-circle-up fa-2x"></i></a>
        </div>
    </div>
    
<?php else: ?>

<div class="row">
  <?php if( have_rows('content_left') ): ?>
    <div class="col-sm-6">
        <?php while (have_rows('content_left')) : the_row(); ?>
        <div class="block<?php if(get_sub_field('bordered')) echo ' bordered'; ?>">
            
      <?php if( get_row_layout() == 'content_block' ): ?>
            
        <?php the_sub_field('content'); ?>
                
      <?php elseif( get_row_layout() == 'image_gallery' ): ?>
            
                <div class="row row-no-padding">
                <?php 
                    if( have_rows('image') ):
                    while ( have_rows('image') ) : the_row();
                ?>
                <div class="col-sm-6">
                    <a class="confirm" href="<?php the_sub_field('link'); ?>" target="_blank"><img class="img-responsive" src="<?php the_sub_field('image'); ?>" /></a>
                </div>            
                <?php endwhile;endif; ?>
                </div>
                
            <?php elseif( get_row_layout() == 'accordian' ): ?>
              <div class="accordian">
                  <div class="intro">
                  <?php the_sub_field('intro'); ?>
                    </div>
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php if(have_rows('accordian_segment')): $count = 0;
            while (have_rows('accordian_segment')) : the_row(); ?>
                  
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><a data-toggle="collapse"<?php if($count != 0) echo ' class="collapsed"' ?> data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a></h3>
                            </div>
                            <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse <?php if($count != 0) { echo 'collapsed'; } else { echo 'in'; } ?>">
                                <div class="panel-body">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            </div>
                        </div>
                        
                    <?php $count++;endwhile;endif; ?>
                    </div>
                </div>
      
      <?php endif; ?>
            
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
    
    <?php if( have_rows('content_right') ): ?>
    <div class="col-sm-6">
        <?php while (have_rows('content_right')) : the_row(); ?>
        <div class="block<?php if(get_sub_field('bordered')) echo ' bordered'; ?>">
        
            <?php if( get_row_layout() == 'content_block' ): ?>
            
        <?php the_sub_field('content'); ?>
                
            <?php  elseif( get_row_layout() == 'image_gallery' ): ?>
            
                <div class="row row-no-padding">
                <?php 
                    if( have_rows('image') ):
                    while ( have_rows('image') ) : the_row();
                ?>
                <div class="col-sm-6">
                    <a class="confirm" href="<?php the_sub_field('link'); ?>" target="_blank"><img class="img-responsive" src="<?php the_sub_field('image'); ?>" /></a>
                </div>            
                <?php endwhile;endif; ?>
                </div>
            <?php elseif( get_row_layout() == 'accordian' ): ?>
              <div class="accordian">
                  <div class="intro">
                  <?php the_sub_field('intro'); ?>
                    </div>
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php if(have_rows('accordian_segment')): $count = 0;
            while (have_rows('accordian_segment')) : the_row(); ?>
                  
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><a data-toggle="collapse"<?php if($count != 0) echo ' class="collapsed"' ?> data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a></h3>
                            </div>
                            <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse <?php if($count != 0) { echo 'collapsed'; } else { echo 'in'; } ?>">
                                <div class="panel-body">
                                    <?php the_sub_field('content'); ?>
                                </div>
                            </div>
                        </div>
                        
                    <?php $count++;endwhile;endif; ?>
                    </div>
                </div>
      
            <?php endif; ?>
        </div>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</div>

<?php endif; ?>

<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>