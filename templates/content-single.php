<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="entry-content">
      <?php the_content(); ?>
      <?php if( have_rows('source') ): ?>
      <div id="sources">
      	<div class="src-head">
            <h3>Sources:</h3>
            <a target="_blank" href="<?php bloginfo('url'); ?>/disclaimer/" class="btn btn-primary">Disclaimer</a>
        </div>
        <?php while ( have_rows('source') ) : the_row(); ?>
		<p><i><?php the_sub_field('title'); ?></i><br>
        <a target="_blank" href="<?php the_sub_field('url'); ?>"><?php the_sub_field('url'); ?></a></p>
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
